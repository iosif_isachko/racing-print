import * as config from "./config";
import login from "./game";

export default io => {
	login(io.of('/game'));
};
