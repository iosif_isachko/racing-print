import UsersRepositories from "../repositories/users";
import RoomsRepositories from "../repositories/rooms";


export default io => {
	io.on('connection', (socket) => {
		const username = socket.handshake.query.username;

		const repRooms = new RoomsRepositories(socket);
		const repUsers = new UsersRepositories(socket);

		if (!repUsers.add(username)) {
			socket.emit('ERROR_LOGIN', `Active user with this name "${username}" already exists.`);
		} else {
			socket.on('CREATE_ROOM', repRooms.create);
			socket.on('disconnect', () => repUsers.disconnect(username));
		}

	});
};
