import SetList from '../components/setList';

// REPOSITORIES
const ROOMS = SetList();

// EVENT
const EVENT = {
	ERROR_CREATE_ROOM: 'ERROR_CREATE_ROOM',
	CREATE_ROOM: 'CREATE_ROOM',
};

class RoomsRepositories {
	constructor(socket) {
		this.socket = socket;
	}

	create(data) {
		const { room } = data.room;
		if(!ROOMS.add(room)) {
			this.socket.emit(EVENT.ERROR_CREATE_ROOM, `There is already a room with such a name "${room}".`);
		} else {
			this.socket.emit(EVENT.CREATE_ROOM, room);
		}
	}
}

export default RoomsRepositories;