import SetList from '../components/setList';

// REPOSITORIES
const USERS = SetList();

// EVENT
const EVENT = {};

class UsersRepositories {
	constructor(socket) {
		this.socket = socket;
	}

	add(username) {
		return USERS.add(username)
	}

	disconnect(username) {
		USERS.remove(username);
		// console
		console.log(`Disconnected "${username}"`);
	}
}

export default UsersRepositories;
