const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

const socket = io('/game', { query: { username } });



// ~~~~~~~~~~~ ERROR FUNCTIONS
const errorAlert = (mes) => alert(mes);

const errorLogin = (mes) => {
	errorAlert(mes);
	sessionStorage.clear();
	window.location.replace('/login');
};



// FUNCTION
function createRoom(roomName){
	console.log(roomName);
}



// EVENT HANDLER
function createRoomHandler() {
	const message = 'Enter the name of the room:';
	const room = window.prompt(message);
	if (room) {
		socket.emit('CREATE_ROOM', { room });
	}
};




// ~~~~~~~~~~~ CRETE ROOM
const createRoomBtn = document.getElementById('add-room-btn');
createRoomBtn.addEventListener('click', createRoomHandler);



// ~~~~~~~~~~~ SOCKET on
socket.on('CREATE_ROOM', createRoom);

// ~~~~~~~~~~~ SOCKET on ERROR
socket.on('ERROR_LOGIN', errorLogin);
socket.on('ERROR_CREATE_ROOM', errorAlert);

