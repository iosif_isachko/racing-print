// SET LIST

export default SetList => {
	const setList = new Set();

	// ADD
	const add = (value) => {
		if (setList.has(value)) {
			return false;
		}
		setList.add(value);
		return true;
	};

	// REMOVE
	const remove = value => {
		setList.delete(value);
	};

	return {
		add,
		remove,
	};
}
